<?php

namespace Drupal\commerce_epaybg\Plugin\Commerce\PaymentMethodType;

use Drupal\commerce_payment\Entity\PaymentMethodInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentMethodType\PaymentMethodTypeBase;

/**
 * Provides the EpayBG Checkout payment method type.
 *
 * @CommercePaymentMethodType(
 *   id = "epay_checkout",
 *   label = @Translation("Epay Checkout"),
 *   create_label = @Translation("Epay"),
 * )
 */
class EpayBGType extends PaymentMethodTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildLabel(PaymentMethodInterface $payment_method) {
    return $this->t('Epay BG Payment Method Type');
  }

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    $fields = parent::buildFieldDefinitions();

    return $fields;
  }

}
