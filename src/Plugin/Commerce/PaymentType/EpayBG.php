<?php

namespace Drupal\commerce_epaybg\Plugin\Commerce\PaymentType;

use Drupal\commerce_payment\Plugin\Commerce\PaymentType\PaymentTypeBase;

/**
 * Provides the EPAYBG payment type.
 *
 * @CommercePaymentType(
 *   id = "epaybg_payment_type",
 *   label = @Translation("Epay BG payment Type"),
 *   workflow = "payment_epaybg",
 * )
 */
class EpayBG extends PaymentTypeBase {

  /**
   * {@inheritdoc}
   */
  public function buildFieldDefinitions() {
    return [];
  }

}
