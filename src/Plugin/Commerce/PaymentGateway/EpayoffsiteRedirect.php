<?php

namespace Drupal\commerce_epaybg\Plugin\Commerce\PaymentGateway;

use Drupal\commerce_order\Entity\OrderInterface;
use Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayBase;
use Drupal\commerce_price\MinorUnitsConverterInterface;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Core\Messenger\Messenger;
use Drupal\commerce_payment\PaymentMethodTypeManager;
use Drupal\commerce_payment\PaymentTypeManager;
use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;
use Drupal\commerce_epaybg\EpayConnectionService;
use Drupal\Core\Database\Connection;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;

/**
 * Provides the Off-site Redirect payment gateway.
 *
 * @CommercePaymentGateway(
 *   id = "EpayBG_offsite_redirect",
 *   label = "EpayBG (Redirect to EpayBG system)",
 *   display_label = "EpayBG",
 *    forms = {
 *     "offsite-payment" = "Drupal\commerce_epaybg\PluginForm\EpayoffsiteRedirect\EpaypaymentOffsiteForm",
 *   },
 *   ayment_method_types = {"epay_checkout"},
 *   payment_type = "epaybg_payment_type",
 *   requires_billing_information = FALSE,
 * )
 */
class EpayoffsiteRedirect extends OffsitePaymentGatewayBase {

  /**
   * EpayConnectionService definition dependency.
   *
   * @var \Drupal\commerce_epaybg\EpayConnectionService
   */
  protected $epayConnection;

  /**
   * Messenger definition dependency.
   *
   * @var \Drupal\Core\Messenger\Messenger
   */
  protected $messenger;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The logger factory.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public function __construct(array $configuration,
                              $plugin_id,
                              $plugin_definition,
                              EntityTypeManagerInterface $entity_type_manager,
                              PaymentTypeManager $payment_type_manager,
                              PaymentMethodTypeManager $payment_method_type_manager,
                              TimeInterface $time,
                              Messenger $messenger,
                              EpayConnectionService $epay_connection,
                              Connection $database,
                              LoggerChannelFactoryInterface $logger_factory,
                              MinorUnitsConverterInterface $minor_units_converter
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $entity_type_manager, $payment_type_manager, $payment_method_type_manager, $time, $minor_units_converter);
    $this->messenger = $messenger;
    $this->epayConnection = $epay_connection;
    $this->database = $database;
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('entity_type.manager'),
      $container->get('plugin.manager.commerce_payment_type'),
      $container->get('plugin.manager.commerce_payment_method_type'),
      $container->get('datetime.time'),
      $container->get('messenger'),
      $container->get('commerce_epaybg.connection'),
      $container->get('database'),
      $container->get('logger.factory'),
      $container->get('commerce_price.minor_units_converter')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'redirect_method' => 'post',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    // A real gateway would always know which redirect method should be used.
    $form['epay_data'] = [
      'epay_desc_phrase' => [
        '#type' => 'textfield',
        '#title' => $this->t('Epay description phrase'),
        '#default_value' => (isset($this->configuration['epay_data']['epay_desc_phrase'])) ? $this->configuration['epay_data']['epay_desc_phrase'] : '',
        '#size' => 60,
        '#maxlength' => 50,
        '#required' => TRUE,
      ],
      // Live mode EpayBG Fields.
      'live_email' => [
        '#type' => 'email',
        '#title' => $this->t('Epay User Email'),
        '#default_value' => (isset($this->configuration['epay_data']['live_email'])) ? $this->configuration['epay_data']['live_email'] : '',
        '#size' => 60,
        '#maxlength' => 50,
        '#states' => [
          'visible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'live'],
          ],
          'invisible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'test'],
          ],
          'required' => [
            ':input[name="configuration[mode]"]' => ['value' => 'live'],
          ],
        ],
      ],
      'live_min' => [
        '#type' => 'textfield',
        '#title' => $this->t('Epay User ID(min)'),
        '#default_value' => (isset($this->configuration['epay_data']['live_min'])) ? $this->configuration['epay_data']['live_min'] : '',
        '#size' => 60,
        '#maxlength' => 15,
        '#states' => [
          'visible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'live'],
          ],
          'invisible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'test'],
          ],
          'required' => [
            ':input[name="configuration[mode]"]' => ['value' => 'live'],
          ],
        ],
      ],
      'live_key' => [
        '#type' => 'textfield',
        '#title' => $this->t('Epay Secret Key'),
        '#default_value' => (isset($this->configuration['epay_data']['live_key'])) ? $this->configuration['epay_data']['live_key'] : '',
        '#size' => 60,
        '#maxlength' => 75,
        '#states' => [
          'visible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'live'],
          ],
          'invisible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'test'],
          ],
          'required' => [
            ':input[name="configuration[mode]"]' => ['value' => 'live'],
          ],
        ],
      ],
      // Test mode EpayBG Fields.
      'test_email' => [
        '#type' => 'email',
        '#title' => $this->t('Test Epay User Email'),
        '#default_value' => (isset($this->configuration['epay_data']['test_email'])) ? $this->configuration['epay_data']['test_email'] : '',
        '#size' => 60,
        '#maxlength' => 50,
        '#states' => [
          'invisible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'live'],
          ],
          'visible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'test'],
          ],
          'required' => [
            ':input[name="configuration[mode]"]' => ['value' => 'test'],
          ],
        ],
      ],
      'test_min' => [
        '#type' => 'textfield',
        '#title' => $this->t('Test Epay User ID(min)'),
        '#default_value' => (isset($this->configuration['epay_data']['test_min'])) ? $this->configuration['epay_data']['test_min'] : '',
        '#size' => 60,
        '#maxlength' => 15,
        '#states' => [
          'invisible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'live'],
          ],
          'visible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'test'],
          ],
          'required' => [
            ':input[name="configuration[mode]"]' => ['value' => 'test'],
          ],
        ],
      ],
      'test_key' => [
        '#type' => 'textfield',
        '#title' => $this->t('Test Epay Secret Key'),
        '#default_value' => (isset($this->configuration['epay_data']['test_key'])) ? $this->configuration['epay_data']['test_key'] : '',
        '#size' => 60,
        '#maxlength' => 75,
        '#states' => [
          'invisible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'live'],
          ],
          'visible' => [
            ':input[name="configuration[mode]"]' => ['value' => 'test'],
          ],
          'required' => [
            ':input[name="configuration[mode]"]' => ['value' => 'test'],
          ],
        ],
      ],
      'exp_time' => [
        '#type' => 'textfield',
        '#title' => $this->t('Expiration date'),
        '#description' => $this->t('Use relative date. Example: <i>+1 day</i>, <i>+36 hours</i> and etc.'),
        '#default_value' => (isset($this->configuration['epay_data']['exp_time'])) ?
        $this->configuration['epay_data']['exp_time'] : '+30 minutes',
      ],
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    if (!$form_state->getErrors()) {
      $values = $form_state->getValue($form['#parents']);

      // Add payment mode to EpayBG config data.
      $values['epay_data']['mode'] = $this->getMode();

      $this->configuration['epay_data'] = $values['epay_data'];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onReturn(OrderInterface $order, Request $request) {
    try {
      $this->createCommercePayment($order);
      $this->messenger->addMessage(t('EpayBG Payment was processed'));
    }
    catch (\Exception $e) {
      // Logs the exception message.
      $this->loggerFactory->get('commerce_epaybg')->error($e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function onNotify(Request $request) {
    $response_msg = '';

    $mode = $this->configuration['epay_data']['mode'];
    $encoded = $request->get('encoded');
    $checksum = $request->get('checksum');
    $secret = $this->configuration['epay_data'][$mode . '_key'];
    $epay_payment_data = $this->epayConnection->commerceEpaybgReceiveData($encoded, $checksum, $secret);
    $commerce_order_storage = NULL;

    if (!empty($epay_payment_data)) {
      try {
        $commerce_order_storage = $this->entityTypeManager->getStorage('commerce_order');
      }
      catch (\Exception $e) {
        // Logs the exception message.
        $this->loggerFactory->get('commerce_epaybg')->error($e->getMessage());
      }

      foreach ($epay_payment_data as $epay_payment) {
        try {
          $epay_db_obj = $this->database->select('commerce_epaybg_payments')
            ->condition('invoice', $epay_payment['invoice'])
            ->fields('commerce_epaybg_payments', ['commerce_order_id'])
            ->execute()
            ->fetchObject();
          if (
            !empty($epay_db_obj->commerce_order_id) &&
            $commerce_order_storage instanceof EntityStorageInterface
          ) {
            /** @var \Drupal\commerce_order\Entity\OrderInterface $commerce_order */
            $commerce_order = $commerce_order_storage->load($epay_db_obj->commerce_order_id);

            /** @var \Drupal\commerce_payment\Entity\PaymentInterface $commerce_payment */
            $commerce_payment = $this->createCommercePayment($commerce_order);
            $transitions = [
              'PAID' => 'authorize_capture',
              'DENIED' => 'void',
              'EXPIRED' => 'expire',
            ];

            if (isset($transitions[$epay_payment['status']])) {
              $payment_transition = $commerce_payment->getState()
                ->getWorkflow()
                ->getTransition($transitions[$epay_payment['status']]);
              $commerce_payment->getState()->applyTransition($payment_transition);
            }

            $commerce_payment->setRemoteState($epay_payment['status']);
            $commerce_payment->save();
            $commerce_order->save();
          }
        }
        catch (\Exception $e) {
          // Logs the exception message.
          $this->loggerFactory->get('commerce_epaybg')->error($e->getMessage());
        }

        $epay_db_state_fieds = ['commerce_epay_status' => $epay_payment['status']];
        $epay_db_state = $this->database->update('commerce_epaybg_payments')
          ->fields($epay_db_state_fieds)
          ->condition('invoice', $epay_payment['invoice'], '=')
          ->execute();

        if ($epay_db_state) {
          $response_msg .= "INVOICE=" . $epay_payment['invoice'] . ":STATUS=OK\n";
        }
        else {
          $response_msg .= "INVOICE=" . $epay_payment['invoice'] . ":STATUS=ERR\n";
        }
      }
    }
    else {
      $response_msg = "ERR=Not valid CHECKSUM\n";
    }

    $content_type = ['content-type' => 'text/plain'];
    $response = new Response(
      $response_msg,
      Response::HTTP_OK,
      $content_type
    );

    return $response;
  }

  /**
   * Create commerce payment.
   *
   * @param \Drupal\commerce_order\Entity\OrderInterface $commerce_order
   *   The commerce order for whom the payment is.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   The commerce payment.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  private function createCommercePayment(OrderInterface $commerce_order) {
    $epay_db_obj_fields = [
      'invoice',
      'epay_payment_total_price',
      'commerce_epay_status',
    ];
    $epay_db_obj = $this->database->select('commerce_epaybg_payments')
      ->condition('commerce_order_id', $commerce_order->id())
      ->fields('commerce_epaybg_payments', $epay_db_obj_fields)
      ->execute()
      ->fetchObject();

    // @todo Add examples of request validation.
    $payment_storage = $this->entityTypeManager->getStorage('commerce_payment');
    $commerce_payments = $payment_storage->loadByProperties([
      'order_id' => $commerce_order->id(),
      'test' => $this->getMode() == 'test',
      'remote_id' => $epay_db_obj->invoice,
    ]);
    $commerce_payment = !empty($commerce_payments) ? reset($commerce_payments) : NULL;
    if (empty($commerce_payment)) {
      $payment_state = 'new';
      switch ($epay_db_obj->commerce_epay_status) {
        case 'IN PROGRESS':
          $payment_state = 'pending';
          break;

        case 'PAID':
          $payment_state = 'remote_completed';
          break;
      }

      $commerce_payment = $payment_storage->create([
        'state' => $payment_state,
        'amount' => $commerce_order->getTotalPrice(),
        'payment_gateway' => $this->parentEntity->id(),
        'order_id' => $commerce_order->id(),
        'test' => $this->getMode() == 'test',
        'remote_id' => $epay_db_obj->invoice,
        'remote_state' => $epay_db_obj->commerce_epay_status,
        'authorized' => $this->time->getRequestTime(),
      ]);
      $commerce_payment->save();
    }

    return $commerce_payment;
  }

}
