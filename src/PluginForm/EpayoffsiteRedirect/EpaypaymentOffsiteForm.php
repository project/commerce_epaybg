<?php

namespace Drupal\commerce_epaybg\PluginForm\EpayoffsiteRedirect;

use Drupal\commerce_epaybg\EpayConnectionService;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Form\FormStateInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Implementation of BasePaymentOffsiteForm for EpayBG gateway.
 *
 * @package Drupal\commerce_epaybg\PluginForm\EpayoffsiteRedirect
 */
class EpaypaymentOffsiteForm extends BaseEpayOffisiteForm {

  /**
   * The configuration factory.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected $configFactory;

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The Epay Connection Service.
   *
   * @var \Drupal\commerce_epaybg\EpayConnectionService
   */
  protected $epayConnection;

  /**
   * Constructs a new Epay Payment Offsite Form.
   *
   * @param \Drupal\Core\Database\Connection $database
   *   The database connection container.
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The configuration factory.
   * @param \Drupal\commerce_epaybg\EpayConnectionService $epay_connection
   *   The Epay Connection Service object.
   */
  public function __construct(Connection $database, ConfigFactoryInterface $config_factory, EpayConnectionService $epay_connection) {
    $this->database = $database;
    $this->configFactory = $config_factory;
    $this->epayConnection = $epay_connection;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('config.factory'),
      $container->get('commerce_epaybg.connection')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);

    /** @var \Drupal\commerce_payment\Entity\PaymentInterface $payment */
    $payment = $this->entity;

    /** @var \Drupal\commerce_payment\Plugin\Commerce\PaymentGateway\OffsitePaymentGatewayInterface $payment_gateway_plugin */
    $payment_gateway_plugin = $payment->getPaymentGateway()->getPlugin();
    $epay_config_data = $payment_gateway_plugin->getConfiguration()['epay_data'];
    $epay_mode = $epay_config_data['mode'];
    $epay_exp_time = !empty($epay_config_data['exp_time']) ?
      $epay_config_data['exp_time'] : '+30 minutes';
    $config = $this->configFactory->get('commerce_epaybg.settings');

    $redirect_url = $config->get('commerce_epaybg_settings.' . $epay_mode . '_url');
    $redirect_method = $config->get('commerce_epaybg_settings.method');

    $epay_enc_data = $this->epayConnection->commerceEpaybgCreatePostData(
      $epay_config_data[$epay_mode . '_key'],
      $epay_config_data[$epay_mode . '_min'],
      $payment->getOrder(),
      $payment->getAmount()->getNumber(),
      $epay_exp_time,
      $epay_config_data['epay_desc_phrase']
    );

    // Add payment data to custom EpayBG db table.
    $this->database->insert('commerce_epaybg_payments')
      ->fields([
        'invoice' => $epay_enc_data['invoice'],
        'commerce_order_id' => $epay_enc_data['order_id'],
        'epay_payment_total_price' => $payment->getAmount()->getNumber(),
      ]
      )->execute();

    $data = [
      'PAGE' => 'paylogin',
      'ENCODED' => $epay_enc_data['encoded'],
      'CHECKSUM' => $epay_enc_data['checksum'],
      'URL_OK' => $form['#return_url'],
      'URL_CANCEL' => $form['#cancel_url'],
    ];

    return $this->buildRedirectForm($form, $form_state, $redirect_url, $data, $redirect_method);
  }

}
