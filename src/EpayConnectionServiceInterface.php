<?php

namespace Drupal\commerce_epaybg;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Manage EpayBG service methods prototypes.
 *
 * @package Drupal\commerce_epaybg
 */
interface EpayConnectionServiceInterface {

  /**
   * Creates epay POST data.
   *
   * @param string $secret
   *   The configured epay secret.
   * @param string $min
   *   The configured epay merchant id.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The requested order.
   * @param string $total
   *   The total amount of the order.
   * @param string $epay_exp_time
   *   The configured expiration time.
   * @param string $desc_msg
   *   The needed description order message.
   *
   * @return array
   *   The processed data.
   */
  public function commerceEpaybgCreatePostData($secret, $min, OrderInterface $order, $total, $epay_exp_time, $desc_msg = 'Test'): array;

  /**
   * Receives and decrypt epay POST data.
   *
   * @param string $encoded
   *   The encoded data.
   * @param string $checksum
   *   The checksum epay data.
   * @param string $secret
   *   The configured epay secret.
   *
   * @return array
   *   The processed data.
   */
  public function commerceEpaybgReceiveData($encoded, $checksum, $secret): array;

}
