<?php

namespace Drupal\commerce_epaybg;

use Drupal\commerce_order\Entity\OrderInterface;

/**
 * Service for EpayBg API call implementation.
 *
 * @package Drupal\commerce_epaybg
 */
class EpayConnectionService implements EpayConnectionServiceInterface {

  /**
   * Creates hmac encrypted string.
   *
   * @param string $algo
   *   The requested algorithm.
   * @param string $data
   *   The encoded data.
   * @param string $passwd
   *   Epay config password.
   *
   * @return mixed
   *   Returned encoded data.
   */
  protected function commerceEpaybgHmac($algo, $data, $passwd) {
    // Md5 and sha1 only.
    $algo = strtolower($algo);
    $p = [
      'md5' => 'H32',
      'sha1' => 'H40',
    ];

    if (strlen($passwd) > 64) {
      $passwd = pack($p[$algo], $algo($passwd));
    }

    if (strlen($passwd) < 64) {
      $passwd = str_pad($passwd, 64, chr(0));
    }

    $ipad = substr($passwd, 0, 64) ^ str_repeat(chr(0x36), 64);
    $opad = substr($passwd, 0, 64) ^ str_repeat(chr(0x5C), 64);

    return ($algo($opad . pack($p[$algo], $algo($ipad . $data))));
  }

  /**
   * Creates epay POST data.
   *
   * @param string $secret
   *   The configured epay secret.
   * @param string $min
   *   The configured epay merchant id.
   * @param \Drupal\commerce_order\Entity\OrderInterface $order
   *   The requested order.
   * @param string $total
   *   The total amount of the order.
   * @param string $epay_exp_time
   *   The configured expiration time.
   * @param string $desc_msg
   *   The needed description order message.
   *
   * @return array
   *   The processed data.
   */
  public function commerceEpaybgCreatePostData($secret, $min, OrderInterface $order, $total, $epay_exp_time, $desc_msg = 'Test'): array {
    // Invoice number.
    $invoice = $order->id() . rand(10000, 99999);

    // The amount of the invoice.
    $sum = sprintf('%.2f', $total);

    $exp_date = date("d.m.Y H:i:s", strtotime($epay_exp_time));
    $descr = $desc_msg . $invoice;

    $data = <<<DATA
MIN={$min}
INVOICE={$invoice}
AMOUNT={$sum}
EXP_TIME={$exp_date}
DESCR={$descr}
DATA;

    $encoded = base64_encode($data);
    // SHA-1 algorithm REQUIRED.
    $checksum = $this->commerceEpaybgHmac('sha1', $encoded, $secret);

    $returned_epay_data = [
      'encoded' => $encoded,
      'checksum' => $checksum,
      'order_id' => $order->id(),
      'invoice' => $invoice,
    ];

    return $returned_epay_data;
  }

  /**
   * Receives and decrypt epay POST data.
   *
   * @param string $encoded
   *   The encoded data.
   * @param string $checksum
   *   The checksum epay data.
   * @param string $secret
   *   The configured epay secret.
   *
   * @return array
   *   The processed data.
   */
  public function commerceEpaybgReceiveData($encoded, $checksum, $secret): array {
    // SHA-1 algorithm REQUIRED.
    $hmac = $this->commerceEpaybgHmac('sha1', $encoded, $secret);
    $info_data = [];

    // Checks if the received CHECKSUM is correct.
    if ($hmac == $checksum) {
      $data = base64_decode($encoded);
      $lines_arr = explode("\n", $data);

      foreach ($lines_arr as $line) {
        if (preg_match("/^INVOICE=(\d+):STATUS=(PAID|DENIED|EXPIRED)(:PAY_TIME=(\d+):STAN=(\d+):BCODE=([0-9a-zA-Z]+))?$/", $line, $regs)) {
          $info_data[] = [
            'invoice'   => $regs[1],
            'status'    => $regs[2],
            'pay_date'  => $regs[4] ?? NULL,
            'stan'      => $regs[5] ?? NULL,
            'bcode'     => $regs[6] ?? NULL,
          ];
        }
      }
    }

    return $info_data;
  }

}
