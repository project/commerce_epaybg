# Commerce EpayBG
Provides Commerce Payment functionality for implementing the Bulgarian
Payment system EpayBG.

## Setup

1. Install the module;

2. Go to /admin/commerce/config/payment-gateways and
click on "Add payment gateway" button;

3. Fill the form with the needed credentials and save the form.
Demo credentials can be created in [demo.epay.bg](demo.epay.bg);

4. Go to admin/commerce/config/checkout-flows.
Edit your default "checkout-flow".
Move "Payment process" widget to "Payment" step;
