<?php

namespace Drupal\Tests\rules\Functional;

use Drupal\Tests\BrowserTestBase;

/**
 * Tests that the EpayBG payment plugin is available in Admin UI related page.
 */
class CommerceEpayBGBrowserTestAdminUI extends BrowserTestBase {

  /**
   * Modules to enable.
   *
   * @var array
   */
  protected static $modules = [
    'commerce',
    'commerce_price',
    'commerce_store',
    'commerce_payment',
    'commerce_epaybg',
  ];

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * Tests if the EpayBG plugin is available in "Add payment gateway" page.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function testEpaybgPluginExisting() {
    $account = $this->drupalCreateUser(
      [
        'administer commerce_payment_gateway',
        'administer commerce_payment',
      ]
    );
    $this->drupalLogin($account);

    $this->drupalGet('admin/commerce/config/payment-gateways/add');
    $this->assertSession()->statusCodeEquals(200);

    // Test that there is an empty reaction rule listing.
    $this->assertSession()->pageTextContains('EpayBG (Redirect to EpayBG system)');
  }

}
